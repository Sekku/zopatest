﻿using System;
using System.Collections.Generic;
using ZopaTest.Common;
using ZopaTest.Data;

namespace ZopaTest.Services
{
    /// <summary>
    /// Calculates quotes for loans depending on a list of market offers
    /// </summary>
    public class QuoteService
    {
        private IDataAccess DataReader { get; }
        private List<MarketOffer> Offers { get; set; }

        public QuoteService(IDataAccess dataReader)
        {
            DataReader = dataReader ?? throw new ArgumentNullException(nameof(dataReader));
            Load();
        }

        private void Load()
        {
            Offers = DataReader.Read();
        }
        
        /// <summary>
        /// If there has been any change to the file, the data can be reloaded
        /// </summary>
        public void ReloadData()
        {
            Load();
        }

        /// <summary>
        /// Returns a quote for a requested <paramref name="amount"/>, in x <paramref name="months"/> and
        /// with a frequency of <paramref name="compoundingFrequency"/>
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="months"></param>
        /// <param name="compoundingFrequency"></param>
        /// <returns></returns>
        public Quote GetQuoteFromOffers(decimal amount, int months, int compoundingFrequency)
        {
            if (Offers == null || Offers.Count == 0) return null;

            // Sort is destructive so if we don't want the parameter to be modified, we should make a copy
            // or use Linq OrderBy, but with a O(n) size cost
            // List.Sort uses a QuickSort (O(nlogn))
            Offers.Sort(new DuplicateKeyComparer<MarketOffer>());

            decimal rate = 0;
            decimal accFromOffers = 0;
            bool quoteFound = false;
            for (int i = 0; i < Offers.Count; i++)
            {
                var current = Offers[i];

                // If we can get the amount (or more), then stop searching and go to calculate the rate
                if ((accFromOffers + current.Offer) >= amount)
                {
                    decimal amountLent = amount - accFromOffers;
                    rate += current.Rate * (amountLent / amount);

                    quoteFound = true;
                    break;
                }
                else
                {
                    rate += current.Rate * (current.Offer / amount);
                    accFromOffers += current.Offer;
                }
            }
            if (!quoteFound) return null;

            double monthlyRate = Math.Pow((double)(rate + 1), 1.0 / compoundingFrequency) - 1;

            double monthlyPayment = (double)(((double)amount * monthlyRate) / (1 - Math.Pow((1 + monthlyRate), -months)));
            double totalRepayment = monthlyPayment * months;
            
            return new Quote()
            {
                MonthlyRepayment = (decimal)monthlyPayment,
                Rate = rate,
                RequestedAmount = amount,
                TotalRepayment = (decimal)totalRepayment
            };
        }
    }
}
