﻿using System;
using System.Collections.Generic;

namespace ZopaTest.Common
{
    /// <summary>
    /// Data access interface
    /// </summary>
    public interface IDataAccess
    {
        string FilePath { get; }
        List<MarketOffer> Read();
    }
}
