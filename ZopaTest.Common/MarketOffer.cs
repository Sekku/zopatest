﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace ZopaTest.Common
{
    /// <summary>
    /// Represents a specific offer from the market
    /// </summary>
    public class MarketOffer : IComparable
    {
        /// <summary>
        /// Creates a new MarketOffer from an array of strings. The fields must have the format
        /// [Name, Rate, Offer]
        /// </summary>
        /// <param name="fields"></param>
        /// <returns></returns>
        public static MarketOffer ParseFromFields(string[] fields)
        {
            if (fields == null || fields.Length != 3) throw new ArgumentException("The fields dont comply with the format [Name, Rate, Offer]", nameof(fields));

            if (!decimal.TryParse(fields[1], out decimal rate)) throw new FormatException($"The second field {fields[1]} cannot be converted to decimal");
            if (!decimal.TryParse(fields[2], out decimal offer)) throw new FormatException($"The third field {fields[2]} cannot be converted to decimal");

            return new MarketOffer()
            {
                Name = fields[0],
                Rate = rate,
                Offer = offer
            };
        }
        
        /// <summary>
        /// Custom comparison between two MarketOffer objects. The comparison is made by Rate.
        /// If the two fields are equal, then the Offer fields are compared
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public int CompareTo(object obj)
        {
            if (!(obj is MarketOffer other)) throw new ArgumentException("obj is not of the type MarketOffer", nameof(obj));
            int result = Rate.CompareTo(other.Rate);

            if (result == 0)
                return -Offer.CompareTo(other.Offer); // Handle equality by comparing the offers made in descending order
            else
                return result;
        }

        public string Name { get; set; }
        public decimal Rate { get; set; }
        public decimal Offer { get; set; }
    }
}
