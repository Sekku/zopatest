﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ZopaTest.Common
{
    /// <summary>
    /// A quote for a specific requested loan
    /// </summary>
    public class Quote
    {
        public decimal RequestedAmount { get; set; }
        public decimal Rate { get; set; }
        public decimal MonthlyRepayment { get; set; }
        public decimal TotalRepayment { get; set; }
    }
}
