﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using ZopaTest.Common;

namespace ZopaTest.Data
{
    /// <summary>
    /// Reader for market files in CSV format
    /// </summary>
    public sealed class CsvReader : IDataAccess
    {
        // TODO: This two constans could be changed to properties reading from config
        private const bool SkipFirstLine = true; // Skip the first line if it's a header
        private const char SplitChar = ',';

        // I'm assuming that the name of the lenders is only letters
        private static Regex FieldsExpression = new Regex(@"^[a-z]+,\d+(\.\d+)?,\d+(\.\d+)?$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        /// <summary>
        /// Filepath
        /// </summary>
        public string FilePath { get; }

        public CsvReader(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath)) throw new ArgumentNullException(nameof(filePath), "filePath cannot be null nor empty");
            if (!File.Exists(filePath)) throw new FileNotFoundException($"The file at {filePath} doesn't exist");
            
            FilePath = filePath;
        }

        /// <summary>
        /// Reads the content of the files and parses it into a list of MarketOffer
        /// </summary>
        /// <returns></returns>
        public List<MarketOffer> Read()
        {
            var retValue = new List<MarketOffer>();

            var lines = File.ReadAllLines(FilePath);
            if (lines == null || lines.Length == 0) return retValue;

            for (int i = SkipFirstLine ? 1 : 0; i < lines.Length; i++)
            {
                if (!FieldsExpression.IsMatch(lines[i])) throw new FormatException($"The line {lines[i]} has an incorrect pattern");

                var fields = lines[i].Split(SplitChar);

                retValue.Add(MarketOffer.ParseFromFields(fields));
            }

            return retValue;
        }
    }
}
