﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using ZopaTest.Common;
using ZopaTest.Data;
using ZopaTest.Services;

namespace ZopaTest
{
    class Program
    {
        // TODO: This two constants should be read by configuration or passed through parameters
        private const int Months = 36;
        private const int CompoundingFrequency = 12;

        static void Main(string[] args)
        {
            if (args == null || args.Length != 2)
            {
                Console.WriteLine("Zopa rate calculation sytem - version 0.0.1");
                Console.WriteLine("Usage: ZopaTest.exe <market_file> <loan_amount>");
                Console.WriteLine("loan_amount must be a value between £1000 and £15000, in intervals of £100");
                return;
            }

            string filePath = args[0];
            if (!int.TryParse(args[1], out int amount))
            {
                Console.WriteLine("The amount must be a value between £1000 and £15000, in intervals of £100");
                return;
            }

            if (amount < 1000 || amount > 15000 || amount % 100 != 0)
            {
                Console.WriteLine("The amount must be a value between £1000 and £15000, in intervals of £100");
                return;
            }

            IDataAccess dataReader;
            try
            {
                dataReader = new CsvReader(args[0]);
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine($"Can't locate the file at {filePath}");
                return;
            }

            try
            {
                var service = new QuoteService(dataReader);
                var quote = service.GetQuoteFromOffers(amount, Months, CompoundingFrequency);

                if (quote == null)
                {
                    Console.WriteLine("A quote for the requested amount of money couldn't be found with the supplied market offers");
                    return;
                }

                // As per specification, the currency numbers should be displayed without the group separator
                var culture = new CultureInfo(CultureInfo.CurrentCulture.Name);
                culture.NumberFormat.CurrencyGroupSeparator = string.Empty;

                Console.WriteLine($"Requested amount: {amount.ToString("C0", culture)}"); // Display the amount with no decimals digits
                Console.WriteLine($"Rate: {quote.Rate:P1}");
                Console.WriteLine($"Monthly repayment: {quote.MonthlyRepayment.ToString("C2", culture)}");
                Console.WriteLine($"Total repayment: {quote.TotalRepayment.ToString("C2", culture)}");
            }
            catch (Exception)
            {
                Console.WriteLine("The operation couldn't be completed");

                // TODO: Properly log the message from the exception
            }
        }
    }
}
