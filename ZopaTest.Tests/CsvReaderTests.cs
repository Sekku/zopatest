using System;
using System.Collections.Generic;
using System.IO;
using Xunit;
using ZopaTest.Common;
using ZopaTest.Data;

namespace ZopaTest.Tests
{
    public class CsvReaderTests
    {
        [Fact]
        public void ThrowsWhenFileDontExistAndParametersAreIncorrect()
        {
            Assert.Throws(typeof(ArgumentNullException), () => new CsvReader(null));
            Assert.Throws(typeof(ArgumentNullException), () => new CsvReader("    "));

            string filePath = "Fixtures/any_file.csv";
            Assert.Throws(typeof(FileNotFoundException), () => new CsvReader(filePath));
        }

        [Theory]
        [InlineData("Fixtures/CsvReader/Empty_File.csv", 0)]
        [InlineData("Fixtures/CsvReader/Market_Data_for_Exercise_0Offer.csv", 0)]
        [InlineData("Fixtures/CsvReader/Market_Data_for_Exercise_1Offer.csv", 1)]
        [InlineData("Fixtures/CsvReader/Market_Data_for_Exercise_15Offers.csv", 15)]
        public void ReadsCorrectlyCsvFiles(string filePath, int count)
        {
            var csvReader = new CsvReader(filePath);
            var offers = csvReader.Read();

            Assert.NotNull(offers);
            Assert.IsType<List<MarketOffer>>(offers);
            Assert.Equal(count, offers.Count);
        }

        [Fact]
        public void ReadsCorrectlyCsvVeryLargeFiles()
        {
            var csvReader = new CsvReader("Fixtures/CsvReader/Market_Data_for_Exercise_Very_Large.csv");
            var offers = csvReader.Read();

            Assert.NotNull(offers);
            Assert.IsType<List<MarketOffer>>(offers);
            Assert.Equal(550000, offers.Count);
        }
    }
}
