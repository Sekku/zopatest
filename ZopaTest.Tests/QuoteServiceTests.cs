﻿using System;
using System.Collections.Generic;
using System.Text;
using Xunit;
using ZopaTest.Common;
using ZopaTest.Data;
using ZopaTest.Services;

namespace ZopaTest.Tests
{
    public class QuoteServiceTests
    {
        private const int Months = 36;
        private const int CompoundingFrequency = 12;

        private IDataAccess ExerciseDataReader { get; }
        private IDataAccess LargeExerciseDataReader { get; }
        private IDataAccess EmptyDataReader { get; }

        public QuoteServiceTests()
        {
            ExerciseDataReader = new CsvReader("Fixtures/QuoteService/Market_Data_for_Exercise.csv");
            EmptyDataReader = new CsvReader("Fixtures/QuoteService/Empty_File.csv");
            LargeExerciseDataReader = new CsvReader("Fixtures/QuoteService/Market_Data_for_Exercise_Large.csv");
        }

        [Fact]
        public void ThrowsWhenDataAccessIsNullOrParametersAreIncorrect()
        {
            Assert.Throws(typeof(ArgumentNullException), () => new QuoteService(null));

            var service = new QuoteService(ExerciseDataReader);
        }

        [Fact]
        public void ReturnsNullWhenNoDataIsFound()
        {
            var service = new QuoteService(EmptyDataReader);
            var quote = service.GetQuoteFromOffers(15000, Months, CompoundingFrequency);
            Assert.Null(quote);
        }

        [Theory]
        [InlineData(1000, 0.07, 30.78)]
        [InlineData(2300, 0.08, 71.31)]
        public void ReturnsQuotesCorrectly(int amount, decimal expectedRate, decimal expectedMontlyPayment)
        {
            var service = new QuoteService(ExerciseDataReader);
            var quote = service.GetQuoteFromOffers(amount, Months, CompoundingFrequency);

            Assert.NotNull(quote);
            Assert.IsType<Quote>(quote);
            Assert.Equal(expectedRate, decimal.Round(quote.Rate, 2));
            Assert.Equal(expectedMontlyPayment, decimal.Round(quote.MonthlyRepayment, 2));
        }

        [Theory]
        [InlineData(15000, 0.05, 448.82)]
        [InlineData(1000, 0.05, 29.92)]
        [InlineData(10000, 0.05, 299.21)]
        [InlineData(5500, 0.05, 164.57)]
        public void ReturnsQuotesCorrectlyLargeSet(int amount, decimal expectedRate, decimal expectedMontlyPayment)
        {
            var service = new QuoteService(LargeExerciseDataReader);
            var quote = service.GetQuoteFromOffers(amount, Months, CompoundingFrequency);

            Assert.NotNull(quote);
            Assert.IsType<Quote>(quote);
            Assert.Equal(expectedRate, decimal.Round(quote.Rate, 2));
            Assert.Equal(expectedMontlyPayment, decimal.Round(quote.MonthlyRepayment, 2));
        }
    }
}
